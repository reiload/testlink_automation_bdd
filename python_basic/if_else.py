from os import system
from subprocess import check_output

path1 = r"C:\testApps"
path2 = r"C:\testTools"
query_system =  (check_output('reg query \"HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Environment\" /s'))
query_user =  (check_output('reg query \"HKEY_CURRENT_USER\Environment\" /s'))
reg_system = "reg add \"HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Environment\" /f /v Path /t REG_SZ /d \"%PATH%;{}\"".format(path1)
reg_user = "reg add \"HKEY_CURRENT_USER\Environment\" /f /v Path /t REG_SZ /d \"%PATH%;{}\"".format(path2)


def add_registry(string, query, reg):
    if string in str(query):
        print("Path já se encontra no sistema: {}".format(string))
    else:
        print("Adicionando o caminho ao path:{}".format(string))
        system(reg)

add_registry(path1, query_system, reg_system)
add_registry(path2, query_user, reg_user)