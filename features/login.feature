# -*- coding: utf-8 -*-

Feature: Login
  """
  Temos a feature de login, que vai fazer a autenticacao do usuario no sistema,
  caso o login e a senha seja positivo vai ser feito a autenticacao, caso um dos valores
  esteja errado eh emitido uma mensagem de erro para o usuario.

  | Login | Password |
  | True  | True     |
  | True  | False    |
  | False | True     |
  | False | False    |

  Regra: Nao aceitar os seguintes caracteres especiais ><#'$*\,/ nos campos login e senha.

  """
  @chrome @firefox @ie
  Scenario: Cenario Positivo.
    Given A pagina de login.
    When Digitar o login.
    And Digitar o password.
    And Efetuar a autenticacao.
    Then O menu com o nome  do usuario deve ser exibido.

  Scenario: Cenario Negativo.
    Given A pagina de login.
    When Digitar o login.
    And Digitar o password errado "1234".
    And Efetuar a autenticacao.
    Then Deve ser exibido uma mensagem de erro.
    """
    Try again! Wrong login name or password!
    """
