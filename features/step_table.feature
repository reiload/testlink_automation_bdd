# -*- coding: utf-8 -*-
# language: pt

Funcionalidade: User Management

  Contexto: Login com Admin
    Dado A pagina de login.
    Quando Eu preencho o campo login "<Login_option>".
    E Eu preencho o campo password "<Password_option>".
    E Realizo a autenticacao no sistema.
    Entao Validar o "<Scenario>" Cenario realizado.

  Exemplos:
  | Login_option    | Password_option    | Scenario |
  | True            | True               | Positivo |

  Cenario: Criar um usuario com perfil de leader.
    Dado a pagina gerenciamento de usuario.
    E entrar na pagina de cadastro de usuario.
    E o combo box com os perfis de usuarios.
      | user_type     |
      | tester        |
      | guest         |
      | leader        |
      | senior tester |
      | test designer |
    Quando eu preencher os dados com o perfil "leader".
    E efetuar o cadastro.
    Entao o novo user deve ter acesso a todas as funcionalidades.
      | Menu               |
      | Test Project       |
      | Test Specification |
      | Test Plan          |
      | Test Execution     |