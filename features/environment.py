from helpers.page import PageBase
from helpers.browser import set_browser
from time import sleep


# def before_all(context):
#     context.driver = set_browser("chrome")
#     context.page = PageBase(context.driver)

def before_scenario(context, scenario):
    context.driver = ""
    if "chrome" in scenario.tags:
        context.driver = set_browser("chrome")
    elif "firefox" in scenario.tags:
        context.driver = set_browser("firefox")
    elif "ie" in scenario.tags:
        context.driver = set_browser("ie")
    if not context.driver == "":
        context.page = PageBase(context.driver)


def after_step(context, step):
    if step.status == "failed":
        context.driver.get_screenshot_as_file \
            ('C:\\testlink_automation\\reports\\ \
            {}_{}.png'.format(step.name, step.status))


def after_scenario(context, scenario):
    if not context.driver == "":
        context.page.close_browser()
