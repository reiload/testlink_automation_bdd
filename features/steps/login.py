from behave import given, when, then, step
from pages.target_elements import TargetElements as El
from pages.page_login import login, password, submit, check_out


@given('A pagina de login.')
def step_impl(context):
    context.page.open(El.Constantes.LOGIN_PAGE)

@when(u'Eu preencho o campo login "{user}".')
def step_impl(context, user):
    user = user.lower()
    context.login = login(context.page, user)

@when(u'Eu preencho o campo password "{Password}".')
def step_impl(context, Password):
    Password = Password.lower()
    context.password = password(context.page, Password)
    assert context.password == True


@when(u'Realizo a autenticacao no sistema.')
def step_impl(context):
    context.submit = submit(context.page)


@then('Validar o "{Scenario}" Cenario realizado.')
def step_impl(context, Scenario):
    Scenario = Scenario.lower()
    context.check_out = check_out(context.page, Scenario)
    assert context.check_out == True


@when(u'Digitar o password errado "{Password}".')
def step_impl(context, Password):
    print(u'Password errado {}, tipo de dado {}.'.format(Password, type(Password)))


@then(u'Deve ser exibido uma mensagem de erro.')
def step_impl(context):
    print("Resposta: {}".format(context.text))



