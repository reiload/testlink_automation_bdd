from behave import given, when, then, step
from pages.target_elements import TargetElements as El
from pages.page_login import login, password, submit, check_out
#
# def before_all(context):
#     driver = set_browser("chrome")
#     context.page = PageBase(driver)
#
# def after_all(context):
#     sleep(10)
#     context.page.close_browser()


# @when(u'Eu preencho o campo login "{user}".')
# def step_impl(context, user):
#     user = user.lower()
#     context.login = login(context.page, user)
#
# @when(u'Eu preencho o campo password "{pass_status}".')
# def step_impl(context, pass_status):
#     pass_status = pass_status.lower()
#     context.password = password(context.page, pass_status)
#     assert context.password == True
#
# @when(u'Realizo a autenticacao no sistema.')
# def step_impl(context):
#     context.submit = submit(context.page)
#
# @then(u'Validar o "{Scenario}" cenario realizado.')
# def step_impl(context, Scenario):
#     context.check_out = check_out(context.page, Scenario)
#     assert context.check_out == True
