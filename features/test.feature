Feature: Login
"""
  Temos a feature de login, que vai fazer a autencacao do usuario no sistema,
  caso o login e a senha seja positivo vai ser feito a autenticacao, caso um dos valores
  seja errado eh emitido uma mensagem de erro para o usuario.

  | Login | Password |
  | True  | True     |
  | True  | False    |
  | False | True     |
  | False | False    |

  Regra: Nao aceitar os seguintes caracteres especiais nos campos login e senha.
"""

  @firefox
  Scenario Outline: Login "<Login_option>" e Password "<Password_option>"
    Given A pagina de login.
    When Eu preencho o campo login "<Login_option>".
    And Eu preencho o campo password "<Password_option>".
    And Realizo a autenticacao no sistema.
    Then Validar o "<Scenario>" cenario realizado.


  Examples:
  | Login_option  | Password_option     | Scenario |
  | True          | True                | Positivo |
  | True          | False               | Negativo |
  | False         | True                | Negativo |
  | False         | False               | Negativo |