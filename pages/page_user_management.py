import re
from pages.target_elements import TargetElements as El
from selenium.webdriver.support.select import Select

# elementos que queremos encontrar
find = ["loginJustToFixSort=", "user_id="]
result = []


def login_user_id(find, string_test):

    # vai dividir a string em uma lista pelo & e ".
    text = re.split('[&"]', string_test)
    print(text)
    # vai comparar os elementos das duas listas.
    # depois vai pegar o resultado separando pelo "=".
    for item in text:
        for x, value in enumerate(find):
            if find[x] in item:
                result.append(item.split("="))

    test = [item.split("=") for item in text
            for x, value in enumerate(find)
            if find[x] in item]

    return test

# exemplo
string_test = u"<a class="" href=\"http://192.168.15.221/lib/usermanagement/usersEdit.php?doAction=edit&loginJustToFixSort=maria_joaquina&user_id=2\">maria_joaquina</a>"
print(login_user_id(find, string_test))
