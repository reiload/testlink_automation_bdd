from pages.settings_page.constantes import ConstantesTestLink


class TargetElements:

    Constantes = ConstantesTestLink()
    # A classe target elements contem os locators das nossas paginas.
    # cada pagina contem um dicionario de elementos.

    login_page = {
        'user': 'user',
        'user_true': 'user',
        'user_false': 'user123',
        'pass': 'bitnami',
        'pass_true': 'bitnami',
        'pass_false': 'bitnami123',
        'login_field': 'id=tl_login',
        'password_field': 'id=tl_password',
        'log_in_button': 'css=input[type="submit"]',
        'feedback': 'css=div.user__feedback'

    }

    main_menu_page = {
        'menu_bar': 'css=div.menu_bar'

    }