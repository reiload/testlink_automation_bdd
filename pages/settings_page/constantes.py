

class ConstantesTestLink:

    BASE_URL = "http://192.168.15.221"

    LOGIN_PAGE = BASE_URL + "/login.php?viewer="
    MAIN_MENU = BASE_URL + "/index.php?caller=login&viewer="
    INDEX_MENU = BASE_URL + "//index.php?caller=login"
    LOGOUT_PAGE = BASE_URL + "/logout.php"
    MAIN_PAGE = BASE_URL + "/lib/general/mainPage.php"

    # Delete page
    DELETE_PROJECT = BASE_URL + "/lib/project/projectEdit.php?doAction=doDelete&tprojectID="
    DELETE_TESTPLAN = BASE_URL + "/lib/plan/planEdit.php?do_action=do_delete&tplan_id="
    DELETE_BUILD = BASE_URL + "/lib/plan/buildEdit.php?do_action=do_delete&build_id="

    # Main tests
    LANGUAGE_MENU = BASE_URL + "/lib/usermanagement/userInfo.php"
    TEST_PLAN_PAGE = BASE_URL + "/lib/plan/planView.php"
    TEST_PROJECT = BASE_URL + "/lib/project/projectView.php"
    TEST_BUILD_PAGE = BASE_URL + "/lib/plan/buildView.php"

    TOP_TEXT_MAIN_PAGE = "TestLink 1.9.13 (Stormbringer)"
    XPATH_TOP_TEXT_MAIN_PAGE = BASE_URL + "/html/body/div[2]/span[3]"
    SCREEN_SAVE= "sanity_test\\screenshots\\"
