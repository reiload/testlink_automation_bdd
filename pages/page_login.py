from pages.target_elements import TargetElements as El


def login(page, login):
    return page.type(El.login_page['login_field'], El.login_page['user_{}'.format(login)])


def password(page, password):
    return page.type(El.login_page['password_field'], El.login_page['pass_{}'.format(password)])


def submit(page):
    return page.submit(El.login_page['log_in_button'])


def check_out(page, Scenario):

    if Scenario == 'positivo':
        page.wait_for_page_change()
        page.change_frame("titlebar")
        print("Esperando o elemento menu_bar")
        result = page.wait_for_visible(El.main_menu_page['menu_bar'])
    else:
        result = page.wait_for_visible(El.login_page['feedback'])
    return result
