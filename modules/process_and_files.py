# coding=utf-8
import threading
import traceback
import sys, os
from time import *
from datetime import datetime
from subprocess import Popen, PIPE, CREATE_NEW_CONSOLE
from modules.json_read import JsonClass


class Process(object):

    """
        Mini Interface executa os testes lendo de um .Json, o teste selecionado eh executado dentro de uma thread que
         tem um time pra finalizar, o teste *.py que é executado em um subprocesso, e esse subprocesso tem um time
          que passado eh morto de forma recursiva.
    """

    def __init__(self):
        self.output = ""
        self.process = ""
        self.error = ""
        self.pid = ""

    def log(self, name, string):
        nome = name
        path_root = os.getcwd()
        path = os.path.join(path_root + "\\Results\\", nome + str(datetime.now().strftime("%Y-%b-%d_%H-%M")) + ".txt")

        with open(path, 'w+') as file_log:
            file_log.write(str(string))
            file_log.close()
        print ("Log Criado: " + str(path))

    def run_process(self, command, file=None):


        if not file:
            self.process = Popen([command], shell=True, creationflags=CREATE_NEW_CONSOLE,
                                 stdin=PIPE, stdout=PIPE, stderr=PIPE, )
            self.error, self.output = self.process.communicate()
            self.process.wait()
        else:
            dic = self.find_dir_file()
            file_path = (dic[file])
            self.process = Popen([file_path], shell=True, creationflags=CREATE_NEW_CONSOLE,
                                 stdin=PIPE, stdout=PIPE, stderr=PIPE, )

        self.pid = self.process.pid
        return self.process, self.pid

        #self.error, self.output = self.process.communicate()
        #self.status = self.process.returncode
        #self.process.terminate()


    # Retorna um DIC com todos os arquivos no diretorio informado.
    def find_dir_file(self):
        dic_files = {}
        root_path = "c:\{}".format(os.getcwd().split('\\')[1])
        for path, subdirs, files in os.walk(root_path):
            if ".git" not in path and ".idea" not in path:
                for file in files:
                    #path.split('\\')[-1]
                    dic_files.update({file:os.path.join(path,file)})
        return dic_files


    def get_duration(self, start):
        end = time()
        hours, rem = divmod(end - start, 3600)
        minutes, seconds = divmod(rem, 60)
        return str("{:0>2}:{:0>2}:{:05.2f}".format(int(hours), int(minutes), seconds))


if __name__ == '__main__':
    #pass
    obj = Process()
    dic = (obj.find_dir_file())
    print(dic['node_chrome.bat'])

