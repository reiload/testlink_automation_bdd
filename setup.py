from distutils.core import setup

setup(
    name='testlink_automation',
    version='1.0',
    packages=['pages', 'pages.settings_page', 'helpers', 'helpers.elements', 'modules', 'features', 'features.steps',
              'test_data'],
    url='www.vidadetestador.com',
    license='',
    author='reiload',
    author_email='reiload@gmail.com',
    description='project automation'
)
